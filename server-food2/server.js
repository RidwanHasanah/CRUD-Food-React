const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const Datastore = require('nedb');

// make variable for express()
const app = express();

app.use(cors()); //this is for cors
app.use(bodyParser.json());

// Make Database
const db = {}
db.users = new Datastore({filename: '.data/users.json', autoload: true});
db.restaurants = new Datastore({filename: '.data/restaurants.json', autoload: true});
db.foods = new Datastore({filename: '.data/foods.json', autoload: true})

app.get('/', (request, response)=>{
    response.send('Good Job');
});

// === List Data ===
app.get('/api/users', (req,res)=>{
    db.users.find({},function(err,docs){
        res.send(docs)
    });
})

// === Insert Data ===
app.post('/api/users', (req,res)=>{
    console.log(req,res);

    const data = {
        "name" : req.body.name,
        "hp" : req.body.hp,
        "email" : req.body.email,
        "gender" : req.body.gender,
        "pass" : req.body.pass,
    }

    db.users.insert(data, (err,docs)=>{
        if(err){
            res.send('error');
            return;
        }

        res.send(docs);
    })
})

// === Update Data ==
app.put('/api/users/:id', (req,res)=>{
    console.log(req.body)
    const data = {
        "name" : req.body.name,
        "hp" : req.body.hp,
        "email" : req.body.email,
        "gender" : req.body.gender,
        "pass" : req.body.pass,
    }

    db.users.update({_id: req.params.id}, data, {}, function(err,numReplace){
        res.send('update completed')
    });
})

// === Delete data ===
app.delete('/api/users/:id', (req,res)=>{
    db.users.remove({_id: req.params.id}, {}, function(req,numReplace){
        res.send('Deleted Success');
    })
})

// === detail data === 
app.get('/api/users/:id', (req,res)=>{
    db.users.find({_id: req.params.id}, function(err,docs){
        res.send(docs)
    })
})

// === Login User === Not fixed
app.get('/api/users/login',(req,res)=>{
    const data = {
        "email" : req.body.email,
        "pass" : req.body.pass,
    }

    db.users.find({$and:[{email:data.email},{pass: data.pass}]}, function(err,docs){
        if (err) {
            res.send('Email or password not match')
            return;
        }
        res.send("You are Logged");
    })
})

/*
=======================================
============= Reaturant ===============
=======================================
*/
// === List Data ===
app.get('/api/restaurants', (req,res)=>{
    db.restaurants.find({},function(err,docs){
        res.send(docs)
    });
})

// === Insert Data ===
app.post('/api/restaurants', (req,res)=>{
    console.log(req,res);

    const data = {
        "name" : req.body.name,
        "hp" : req.body.hp,
        "email" : req.body.email,
        "address" : req.body.address,
        "img" : req.body.img
    }

    db.restaurants.insert(data, (err,docs)=>{
        if(err){
            res.send('error');
            return;
        }

        res.send(docs);
    })
})

// === Update Data ==
app.put('/api/restaurants/:id', (req,res)=>{
    console.log(req.body)
    const data = {
        "name" : req.body.name,
        "hp" : req.body.hp,
        "email" : req.body.email,
        "address" : req.body.address,
        "img" : req.body.img
    }

    db.restaurants.update({_id: req.params.id}, data, {}, function(err,numReplace){
        res.send('update completed')
    });
})

// === Delete data ===
app.delete('/api/restaurants/:id', (req,res)=>{
    db.restaurants.remove({_id: req.params.id}, {}, function(req,numReplace){
        res.send('Deleted Success');
    })
})

// === detail data === 
app.get('/api/restaurants/:id', (req,res)=>{
    db.restaurants.find({_id: req.params.id}, function(err,docs){
        res.send(docs)
    })
})

/**
 * 
=======================================
================= Foods ===============
=======================================
 * 
 */
// === List Data ===
app.get('/api/restaurants/:id_resto/foods', (req,res)=>{
    db.foods.find({},function(err,docs){
        res.send(docs)
    });
})

// === Insert Data ===
app.post('/api/restaurants/:id_resto/foods', (req,res)=>{
    console.log(req,res);

    const data = {
        "name" : req.body.name,
        "desc" : req.body.desc,
        "qty" : req.body.qty,
        "img" : req.body.img,
        "price" : req.body.price,
        "id_resto" : req.params.id_resto
    }

    db.foods.insert(data, (err,docs)=>{
        if(err){
            res.send('error');
            return;
        }

        res.send(docs);
    })
})

// === Update Data ==
app.put('/api/restaurants/:id_resto/foods/:id', (req,res)=>{
    console.log(req.body)
    const data = {
        "name" : req.body.name,
        "desc" : req.body.desc,
        "qty" : req.body.qty,
        "img" : req.body.img,
        "price" : req.body.price
    }

    db.foods.update({_id: req.params.id}, data, {}, function(err,numReplace){
        res.send('update foods completed')
    });
})

// === Delete data ===
app.delete('/api/restaurants/:id_resto/foods/:id', (req,res)=>{
    db.foods.remove({_id: req.params.id}, {}, function(req,numReplace){
        res.send('Deleted Foods Success');
    })
})

// === detail data === 
app.get('/api/restaurants/:id_resto/foods/:id', (req,res)=>{
    db.foods.find({_id: req.params.id}, function(err,docs){
        res.send(docs)
    })
})




// ========== This is Port =======
var listener = app.listen(2000,function(){
    console.log('Your App is Listening on Port ==>===>==> ' + listener.address().port)
})