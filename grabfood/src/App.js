import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Layout from './components/Layout';
import Detail from './components/List/DetailList';
import { Route, Switch, BrowserRouter } from 'react-router-dom';
// ===== Admin ====
import Admin from './admin/admin';
// Restaurants Import
import AllRestaurants from './admin/restaurants/AllRestaurants';
import DetailRestaurant from './admin/restaurants/DetailRestaurant';
import UpdateRestaurant from './admin/restaurants/UpdateRestaurant';
import AddResto from './admin/restaurants/AddRestaurant';
// Foods Import
import AllFoods from './admin/foods/AllFoods';
import DetailFood from './admin/foods/DetailFood';
import UpdateFood from './admin/foods/UpdateFood';
import AddFood from './admin/foods/AddFood';

class App extends Component {
  render() {
    return (
      <div>
        <BrowserRouter>
          <div>
            <Switch>
              <Route path="/" component={Layout} exact />
              <Route path="/detail" component={Detail} />
              {/*====== Admin ======*/}
              <Route path="/admin" exact component={Admin} />
              {/* Reaturannts */}
              <Route path="/admin/allrestaurants" component={AllRestaurants} />
              <Route path="/admin/addresto" component={AddResto} />
              <Route path="/admin/detailrestaurant/:id" component={DetailRestaurant} />
              <Route path="/admin/updaterestaurant/:id" component={UpdateRestaurant} />
              {/*Foods  */}
              <Route path="/admin/allfoods/:id_resto" component={AllFoods} />
              <Route path="/admin/:id_resto/addfood" component={AddFood} />
              <Route path="/admin/:id_resto/detailfood/:id" component={DetailFood} />
              <Route path="/admin/:id_resto/updatefood/:id" component={UpdateFood} />
            </Switch>
          </div>
        </BrowserRouter>
      </div>
    );
  }
}

export default App;
