import React, { Component } from 'react';
import Axios from 'axios';
import {} from 'reactstrap';
import {Link} from 'react-router-dom';
class AllRestaurants extends Component {
    state={
        data:[]
    }

    getData = ()=>{
        Axios.get("http://localhost:2000/api/restaurants").then(res=>{
            console.log(res, "<<<<<<=====")
            this.setState({
                data:res.data
            });
        })
    }

    deleteData = _id=>{
        Axios.delete(`http://localhost:2000/api/restaurants/${_id}`).then(res=>{
            this.getData();
        })
    }

    componentDidMount(){
        this.getData();
    }
    render() {
        return (
            <div className="container" >
                <div className="row my-5 justify-content-center">
                    <div className="col col-lg-12 col-sm-12 col-md-12">
                        <Link to="/admin/addresto"><button className="btn btn-lg btn-outline-success" >Add Resto</button></Link>
                        <hr/>
                        <ul className="list-view" >
                            {
                                this.state.data.map(datum=>{
                                    return(
                                    <li className="media my-2">
                                        <img className="mr-3 img-c" src={datum.img} />
                                        <div className="media-body">
                                            <h5 className="mt-0 mb-1"><Link to={`/admin/detailrestaurant/${datum._id}`}>{datum.name}</Link> </h5>
                                            <p className="fsize" >{datum.address}</p>   
                                        </div>
                                        <div>
                                        <Link to={`/admin/updaterestaurant/${datum._id}`} className="btn btn-outline-info" >update</Link>
                                        &nbsp;&nbsp;
                                        <button
                                        onClick={()=>{
                                            this.deleteData(datum._id)
                                        }}
                                        className="btn btn-outline-danger" >Delete</button>
                                        </div>
                                    </li>
                                    )
                                })
                            }
                        </ul>
                    </div>
                </div>
            </div>
        );
    }
}

export default AllRestaurants;