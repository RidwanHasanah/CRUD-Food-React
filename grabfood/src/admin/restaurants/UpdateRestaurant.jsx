import React, { Component } from 'react';
import {Redirect} from 'react-router-dom';
import Axios from 'axios';

class UpdateReataurant extends Component {
    state = {
        data:null,
        name:'',
        hp: '',
        email:'',
        img:'',
        id: '',
        address:'',
        redirect: false
    }
    componentDidMount(){
        Axios.get('http://localhost:2000/api/restaurants/'+this.props.match.params.id).then(res=>{
            this.setState({
                name: res.data[0].name,
                hp: res.data[0].hp,
                email:res.data[0].email,
                img:res.data[0].img,
                address:res.data[0].address
            })
        })
        console.log(this.props.match.params.id)
    }

    putData = id =>{
        Axios.put('http://localhost:2000/api/restaurants/'+id,{
            name:this.state.name,
            hp: this.state.hp,
            email:this.state.email,
            img:this.state.img,
            address:this.state.address
        })
        .then(res=>{
            this.setState({
                name:'',
                hp: '',
                email:'',
                img:'',
                address:'',
                redirect: true
            })
        })
    }

    handleChange = e =>{
        this.setState({
            [e.target.name]: e.target.value
        });
    }

    render() {
        return (
            <div>
                <div className="container">
                    <div className="row my-5">
                        <div className="col col-lg-8 col-md-8">
                             {/* <form> */}
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                    <label for="name">Resto Name</label>
                                    <input
                                    onChange={this.handleChange} value={this.state.name}
                                     type="text" class="form-control" name="name" id="name" placeholder="Resto Name" />
                                    </div>
                                    <div class="form-group col-md-6">
                                    <label for="hp">Telephone</label>
                                    <input
                                    onChange={this.handleChange} value={this.state.hp}
                                     type="number" name="hp" class="form-control" id="hp" placeholder="4567898774" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="address">Address</label>
                                    <input
                                    onChange={this.handleChange} value={this.state.address} 
                                    type="text" name="address" class="form-control" id="address" placeholder="1234 Main St" />
                                </div>
                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input
                                    onChange={this.handleChange} value={this.state.email}
                                    type="link" class="form-control" name="email" id="email" placeholder="example@.gm.com" />
                                </div>
                                <div class="form-group">
                                    <label for="img">Link Image</label>
                                    <input onChange={this.handleChange} value={this.state.img} 
                                    type="link" class="form-control" name="img" id="img" placeholder="https://ghghghgh" />
                                </div>
                                <button
                                onClick={()=>{
                                    this.putData(this.props.match.params.id);
                                }}
                                 type="submit" class="btn btn-outline-primary">Update</button>
                            {/* </form> */}
                            {this.state.redirect ? <Redirect to="/admin/allrestaurants" />:''}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default UpdateReataurant;