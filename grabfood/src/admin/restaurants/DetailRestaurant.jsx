import React, { Component } from 'react';
import {} from 'reactstrap';
import Axios from 'axios';
import logo from '../../logo.svg';
import '../../App.css';
import {Link} from 'react-router-dom';

class DetailRestaurant extends Component {
    state = {
        data:null
    }

    componentDidMount(){
        console.log(this.props.match.params.id)
        Axios.get('http://localhost:2000/api/restaurants/'+this.props.match.params.id)
        .then(res=>{
            this.setState({
                data:res.data
            })
            console.log(this.state.data)
        })
    }
    render() {
        if (this.state.data == null) {
            return (
                <div className="App">
                    <img style={{marginTop: "20%"}} src={logo} className="App-logo" alt="logo" />
                </div>
                )
        }
        return (
            <div className="container" >
                <div className="row justify-content-center">
                    <div className="col col-lg-8 col-md-8 col-sm-8">
                        <h1>{this.state.data[0].name}</h1>
                        <img src={this.state.data[0].img} alt=""/>
                        <ul>
                            <li>{this.state.data[0].hp}</li>
                            <li>{this.state.data[0].email}</li>
                            <li>{this.state.data[0].address}</li>
                        </ul>
                        <Link className="btn btn-outline-info btn-lg" to={`/admin/allfoods/${this.state.data[0]._id}`} >View All Our Foods</Link>
                    </div>
                </div>
            </div>
        );
    }
}

export default DetailRestaurant;