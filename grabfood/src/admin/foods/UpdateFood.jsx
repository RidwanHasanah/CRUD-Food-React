import React, { Component } from 'react';
import {} from 'reactstrap';
import {Link, Redirect} from 'react-router-dom';
import Axios from 'axios';


class UpdateFoods extends Component {
    state = {
        data:null,
        name: '',
        desc:'',
        qty:'',
        price:'',
        img:''

    }

    componentDidMount(){
        Axios.get('http://localhost:2000/api/restaurants/'+this.props.match.params.id_resto+'/foods/'+this.props.match.params.id)
        .then(res=>{
            this.setState({
                name: res.data[0].name,
                desc:res.data[0].desc,
                qty:res.data[0].qty,
                price:res.data[0].price,
                img:res.data[0].img
            })
            console.log(res)
        })
    }

    putData = id =>{
        Axios.put('http://localhost:2000/api/restaurants/'+this.props.match.params.id_resto+'/foods/'+this.props.match.params.id,{
            name: this.state.name,
            desc:this.state.desc,
            qty:this.state.qty,
            price:this.state.price,
            img:this.state.img
        }).then(res=>{
            this.setState({
                name: '',
                desc:'',
                qty:'',
                price:'',
                img:'',
                redirect:true
            })
        })
    }

    handleChange = e =>{
        this.setState({
            [e.target.name]: e.target.value
        });
    }

    render() {
        return (
            <div>
                <div className="container">
                    <div className="row my-5">
                        <div className="col col-lg-8 col-md-8">
                             {/* <form> */}
                                <div className="form-row">
                                    <div className="form-group col-md-6">
                                    <label for="name">Food Name</label>
                                    <input
                                    onChange={this.handleChange} value={this.state.name}
                                     type="text" className="form-control" name="name" id="name" placeholder="Resto Name" />
                                    </div>
                                    <div className="form-group col-md-6">
                                    <label for="desc">Description</label>
                                    <input
                                    onChange={this.handleChange} value={this.state.desc}
                                     type="text" name="desc" className="form-control" id="desc" placeholder="4567898774" />
                                    </div>
                                </div>
                                <div className="form-group">
                                    <label for="qty">Qty</label>
                                    <input
                                    onChange={this.handleChange} value={this.state.qty} 
                                    type="text" name="qty" className="form-control" id="qty" placeholder="1234 Main St" />
                                </div>
                                <div className="form-group">
                                    <label for="price">Price</label>
                                    <input
                                    onChange={this.handleChange} value={this.state.price}
                                    type="link" className="form-control" name="price" id="price" placeholder="example@.gm.com" />
                                </div>
                                <div className="form-group">
                                    <label for="img">Link Image</label>
                                    <input onChange={this.handleChange} value={this.state.img} 
                                    type="link" className="form-control" name="img" id="img" placeholder="https://ghghghgh" />
                                </div>
                                <button
                                onClick={()=>{
                                    this.putData(this.props.match.params.id);
                                }}
                                 type="submit" className="btn btn-outline-primary">Update</button>
                            {/* </form> */}
                            {this.state.redirect ? <Redirect to={`/admin/allfoods/${this.props.match.params.id_resto}`} />:''}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default UpdateFoods;