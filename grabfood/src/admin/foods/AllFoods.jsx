import React, { Component } from 'react';
import Axios from 'axios';
import {Link} from 'react-router-dom';
class AllFoods extends Component {
    state = {
        data:[]
    }

    getData = ()=>{
        Axios.get('http://localhost:2000/api/restaurants/'+this.props.match.params.id_resto+'/foods')
        .then(res=>{
            console.log("Ini Response ==>",res)
            this.setState({
                data:res.data
            })

        })
    }
    deleteData = _id=>{
        Axios.delete(`http://localhost:2000/api/restaurants/${this.props.match.params.id_resto}/foods/${_id}`).then(res=>{
            this.getData();
        })
    }

    componentDidMount(){
        this.getData();
    }
    render() {
        return (
            <div className="container" >
                <h1 className="text-center" >All Foods</h1>
                <Link className="btn btn-outline-success" to={`/admin/${this.props.match.params.id_resto}/addfood`} >Add Food</Link>
                <hr/>

              <div className="row justify-content-center my-5">
                {
                    this.state.data.map(datum=>{
                        return(
                            <div className="col col-lg-3 col-md-3 col-sm-6 col-xs-6">
                                <div className="card">
                                    <img className="card-img-top" src={datum.img} alt={datum.name} />
                                    <div className="card-body">
                                        <h5 className="card-title">{datum.name}</h5>
                                        <p className="card-text">
                                            {datum.desc}
                                            <br/>
                                            <b>Qty : {datum.qty}</b>
                                            <br/>
                                            <b>Price : {datum.price}</b>
                                        </p>
                                        <Link to={`/admin/${this.props.match.params.id_resto}/detailfood/${datum._id}`} className="btn btn-outline-primary">Detail</Link> &nbsp;
                                        <Link to={`/admin/${this.props.match.params.id_resto}/updatefood/${datum._id}`} className="btn btn-outline-warning">Edit</Link> &nbsp;
                                        <button
                                        onClick={()=>{
                                            this.deleteData(datum._id);
                                        }} className="btn btn-outline-danger" >Delete</button>
                                    </div>
                                </div>
                            </div>
                        )
                    })
                }
                  
              </div>  
            </div>
        );
    }
}

export default AllFoods;