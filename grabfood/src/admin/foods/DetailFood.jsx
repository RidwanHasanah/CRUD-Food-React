import React, { Component } from 'react';
import Axios from 'axios';
class DetailFood extends Component {
    state={
        data:null
    }
    componentDidMount(){
        Axios.get('http://localhost:2000/api/restaurants/'+this.props.match.params.id_resto+'/foods/'+this.props.match.params.id)
        .then(res=>{
            console.log('This is Your data Man..===>',res.data)
            this.setState({
                data: res.data
            })
        })
    }
    render() {
        return (
            <div className="container">
                <div className="row justify-content-center">
                <div className="col col-lg-8 col-md-8 col-sm-8">
                    <h1>{this.state.data[0].name}</h1>
                    <img src={this.state.data[0].img} alt={this.state.data[0].name} />
                    <p>{this.state.data[0].desc}</p>
                    <br/>
                    <b>QTY : {this.state.data[0].qty}</b>
                    <br/>
                    <b>Price : {this.state.data[0].price}</b>
                    </div>
                </div>
            </div>
        );
    }
}

export default DetailFood;