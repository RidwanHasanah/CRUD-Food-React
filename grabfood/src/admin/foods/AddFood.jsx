import React, { Component } from 'react';
import Axios from 'axios';
import {} from 'reactstrap';
import {Redirect} from 'react-router-dom';

class AddFood extends Component {
    state={
        data : null,
        name : '',
        desc : '',
        qty : '',
        price : '',
        img : '',
        redirect : false
    }

    postData = ()=>{
        Axios.post('http://localhost:2000/api/restaurants/'+this.props.match.params.id_resto+'/foods',{
            name : this.state.name,
            desc : this.state.desc,
            qty : this.state.qty,
            price : this.state.price,
            img : this.state.img
        }).then(res=>{
            this.setState({
                name : '',
                desc : '',
                qty : '',
                price : '',
                img : '',
                redirect : true
            })
        })
    }

    handleChange = e =>{
        this.setState({
            [e.target.name]: e.target.value
        });
    }

    render() {
        return (
            <div className="container">
                    <div className="row my-5">
                        <div className="col col-lg-8 col-md-8">
                             {/* <form> */}
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                    <label for="name">Food Name</label>
                                    <input
                                    onChange={this.handleChange} value={this.state.name}
                                     type="text" class="form-control" name="name" id="name" placeholder="Resto Name" />
                                    </div>
                                    <div class="form-group col-md-6">
                                    <label for="desc">Description</label>
                                    <input
                                    onChange={this.handleChange} value={this.state.desc}
                                     type="text" name="desc" class="form-control" id="desc" placeholder="isi descriptionn nya" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="qty">Quantity</label>
                                    <input
                                    onChange={this.handleChange} value={this.state.qty} 
                                    type="text" name="qty" class="form-control" id="qty" placeholder="1234 Main St" />
                                </div>
                                <div class="form-group">
                                    <label for="price">Price</label>
                                    <input
                                    onChange={this.handleChange} value={this.state.price}
                                    type="number" class="form-control" name="price" id="price" placeholder="example@.gm.com" />
                                </div>
                                <div class="form-group">
                                    <label for="img">Link Image</label>
                                    <input onChange={this.handleChange} value={this.state.img} 
                                    type="link" class="form-control" name="img" id="img" placeholder="https://ghghghgh" />
                                </div>
                                <button
                                onClick={()=>{
                                    this.postData();
                                }} type="submit" class="btn btn-outline-primary">Add</button>
                            {/* </form> */}
                            {this.state.redirect ? <Redirect to={`/admin/allfoods/${this.props.match.params.id_resto}`} />:''}
                    </div>
                </div>
            </div>
        );
    }
}

export default AddFood;