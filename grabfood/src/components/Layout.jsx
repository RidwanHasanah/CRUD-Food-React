import React, { Component } from 'react';
import 'antd-mobile/dist/antd-mobile.css';
import { NavBar, Icon } from 'antd-mobile';
import Tabsr from './Tabsr';

class Layout extends Component {
	render() {
		return (
			<div>
    <NavBar
      mode="light"
      icon={<Icon type="left" />}
      onLeftClick={() => console.log('onLeftClick')}
      rightContent={[
        <Icon key="0" type="search" style={{ marginRight: '16px' }} />,
        <Icon key="1" type="ellipsis" />,
      ]}
    >Ridwan Food</NavBar>
    <Tabsr />
			</div>
		);
	}
}

export default Layout