import React, { Component } from 'react';
import Axios from 'axios';
import { ListView } from 'antd-mobile';
import '../../css/style.css';
import { Link } from 'react-router-dom'

class Listview1 extends Component {
    state={
        data : []
    }

    // ===== Show Data =====
    getData = ()=>{
        Axios.get("http://localhost:2000/api/data").then(res=>{
            console.log(res,'===========')
            this.setState({
                data: res.data
            });
        })
    }

    // ===== Delete Data ====
    deleteData = _id =>{
        Axios.delete(`http://localhost:2000/api/data/${_id}`).then(res=>{
            this.getData();
        });
    }

    componentDidMount(){
        this.getData();
    }
    render() {
        return (
            <div className="list-view">
                <ul className="list-unstyled">
                    {
                        this.state.data.map(datum=>{
                            return(
                                <li className="media list-food">
                                    <img className="mr-3 img-c" src={datum.img} />
                                    <div className="media-body">
                                    <h5 className="mt-0 mb-1 fsize"><Link to="/detail">{datum.title}</Link> </h5>
                                    <p className="fsize" >{datum.desc}</p>   
                                    </div>
                                    <button
                                    onClick={()=>{this.deleteData(datum._id)}}
                                     className="btn btn-sm btn-outline-danger fsize" >
                                    Delete
                                    </button>
                                </li>
                            )
                        })
                    }
                </ul>
            </div>
        );
    }
}

export default Listview1;