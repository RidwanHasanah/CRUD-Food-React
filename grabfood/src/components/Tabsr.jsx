import React, { Component } from 'react';
import { Tabs, WhiteSpace, Badge } from 'antd-mobile';
import TabHome from './tabs/TabHome';
import TabHome2 from './tabs/TabHome2';
import Listview1 from './List/Listview1';
import InputData from './tabs/InputData';
import { Button } from 'reactstrap';

const tabs = [
  { title: <Badge text={'3'}>Tranportasion</Badge> },
  { title: <Badge text={'10+'}>Food</Badge> },
  { title: <Badge dot>Input</Badge> },
];




class Tabsr extends Component {
	render() {
		return (
			<div>
				<Tabs tabs={tabs} initialPage={1} onChange={(tab, index) => { console.log('onChange', index, tab); }}
			      onTabClick={(tab, index) => { console.log('onTabClick', index, tab); }}
			    >
			      <div>
					<img src="https://pbs.twimg.com/media/DMPlnjyVoAA2yDt.jpg" alt="" style={{ width: '100%', height: '150px'}}
							onLoad={() => {
								// fire window resize event to change height
								window.dispatchEvent(new Event('resize'));
								this.setState({ imgHeight: 'auto' });
							}}
						/>
			      </div>
			      <div>
					<TabHome />
					<Listview1 />
			      </div>
			      <div>
					<img src="https://i.pinimg.com/originals/49/87/a2/4987a2ff6bc93a597ea3e19a49a27bb7.jpg" alt="" style={{ width: '100%', height: '150px'}}
							onLoad={() => {
								// fire window resize event to change height
								window.dispatchEvent(new Event('resize'));
								this.setState({ imgHeight: 'auto' });
							}}
						/>
						<InputData />
			      </div>
			    </Tabs>
			    <WhiteSpace />
			</div>
		);
	}
}

export default Tabsr