import React, { Component } from 'react';
import Axios from 'axios';
import {} from 'reactstrap';
import Listview from '../List/Listview1';

class InputData extends Component {
    state = {
        title : '',
        desc : '',
        img : ''
    }

    // ===== Function Post Data =====
    postData = ()=>{
        Axios.post("http://localhost:2000/api/data/",{
            title: this.state.title,
            desc: this.state.desc,
            img: this.state.img
        }).then(res => {
            this.setState({
                title : '',
                desc : '',
                img: ''
            })
            // this.getData();
        })
    }

    // ===== Handle Change ===
    handleChange = e =>{
        this.setState({
            [e.target.name]: e.target.value
        });
    }

    render() {
        return (
            <div className="row justify-content-center">
                <div className="col col-sm-4" >
                <div className="form-group" >
                    <label>Title</label>
                    <input
                    onChange={this.handleChange} value={this.state.title}
                    id="title" name="title"
                    placeholder="Bakso" type="text" className="form-control" />
                </div>
                <div className="form-group" >
                    <label>Description</label>
                    <input
                    onChange={this.handleChange} value={this.state.desc}
                    id="desc" name="desc"
                    placeholder="Bakso Enak sekali" type="text" className="form-control" />
                </div>
                <div className="form-group" >
                    <label>Image</label>
                    <input
                    onChange={this.handleChange} value={this.state.img}
                    id="img" name="img"
                    placeholder="http://img.com" type="link" className="form-control" />
                </div>
                <button onClick={()=>{
                        this.postData();
                }} 
                className="btn btn-outline-info form-control" >
                    Send
                </button>
                </div>
            </div>
        );
    }
}

export default InputData;