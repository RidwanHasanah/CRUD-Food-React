import React, { Component } from 'react';
import {} from 'reactstrap';
import '../../css/style.css';

class TabHome2 extends Component {
    render() {
        return (
            <div className="container">
                <div className="row">
                <div className="col col-lg-12 col-sm-12 col-xs-12 col-md-12">
                <div id="carouselExampleIndicators" className="carousel slide" data-ride="carousel">
                    <ol className="carousel-indicators">
                        <li data-target="#carouselExampleIndicators" data-slide-to="0" className="active"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                    </ol>
                    <div className="carousel-inner">
                        <div className="carousel-item active">
                        <img className="d-block w-100" 
                        src="http://webview.hidupdigital.com/wp-content/uploads/2018/03/tanggal-tua-banyak-mau-grab-food-lah.jpg" alt="First slide" />
                        </div>
                        <div className="carousel-item">
                        <img className="d-block w-100" 
                        src="https://www.sbs.com.au/food/sites/sbs.com.au.food/files/styles/full/public/20120816_122_Indonesian-Soto-Ayam_text.jpg?itok=Q7B0R1f2" alt="Second slide" />
                        </div>
                        <div className="carousel-item">
                        <img className="d-block w-100" 
                        src="https://www.sbs.com.au/food/sites/sbs.com.au.food/files/styles/full/public/pandan-scented-chicken-curry-2.jpg?itok=YXqiuyhl" alt="Third slide" />
                        </div>
                    </div>
                    <a className="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                        <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span className="sr-only">Previous</span>
                    </a>
                    <a className="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                        <span className="carousel-control-next-icon" aria-hidden="true"></span>
                        <span className="sr-only">Next</span>
                    </a>
                    </div>
                </div>
                </div>
            </div>
        );
    }
}

export default TabHome2;