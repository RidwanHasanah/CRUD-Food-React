import React, { Component } from 'react';
import { Carousel, Button, WhiteSpace, WingBlank } from 'antd-mobile';

class TabHome extends Component {
	state = {
    data: [
      '1', 
      '2', 
      '3'
    ],
    imgHeight: 100,
    slideIndex: 2,
  }
  componentDidMount() {
    // simulate img loading
    setTimeout(() => {
      this.setState({
        data: [
          'http://webview.hidupdigital.com/wp-content/uploads/2018/03/tanggal-tua-banyak-mau-grab-food-lah.jpg',
          'https://www.sbs.com.au/food/sites/sbs.com.au.food/files/styles/full/public/20120816_122_Indonesian-Soto-Ayam_text.jpg?itok=Q7B0R1f2',
          'https://www.sbs.com.au/food/sites/sbs.com.au.food/files/styles/full/public/pandan-scented-chicken-curry-2.jpg?itok=YXqiuyhl'
          ],
      });
    }, 100);
  }
  componentDidUpdate() {
    // After the new child element is rendered, change the slideIndex
    // https://github.com/FormidableLabs/nuka-carousel/issues/327
    if (this.state.slideIndex !== this.state.data.length - 1) {
      /* eslint react/no-did-update-set-state: 0 */
      this.setState({ slideIndex: this.state.data.length - 1 });
    }
  }
  render() {
    return (
      <Carousel
        autoplay={true}
        infinite
        selectedIndex={this.state.slideIndex}
        beforeChange={(from, to) => console.log(`slide from ${from} to ${to}`)}
        afterChange={index => console.log('slide to', index)}
      >
        {this.state.data.map((val, index) => (
          <a
            key={val + index}
            href="http://"
            // style={{ display: 'inline-block', width: '100%', height: this.state.imgHeight }}
          >
            <img
              src={`${val}`}
              alt=""
              style={{ width: '100%'}}
              onLoad={() => {
                // fire window resize event to change height
                window.dispatchEvent(new Event('resize'));
                this.setState({ imgHeight: 'auto' });
              }}
            />
          </a>
        ))}
    </Carousel>
    );
  }
}
export default TabHome